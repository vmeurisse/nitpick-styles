import os
import sys

if "VIRTUAL_ENV" not in os.environ:
    command = ["poetry", "run", "python3", *sys.argv]
    os.execvp(command[0], command)
