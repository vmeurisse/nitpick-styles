#! /usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

import scripts.with_poetry as _  # isort:skip

import itertools
import json
import uuid
from argparse import ArgumentParser, Namespace
from pathlib import Path
from subprocess import PIPE, run
from tempfile import TemporaryDirectory

from argcomplete import autocomplete

_ROOT = Path(__file__).parent.resolve()


def _test(_: Namespace):
    print('Running test...')

    # Nitpick doesn't have a lint feature for configurations.
    # However, `nitpick ls` will check the syntax.
    # This test will create a project with all config files.
    # We also add a file with random name and confirm it is present in `nitpick ls` output to make sure.
    with TemporaryDirectory(dir=_ROOT, prefix='.test.') as tmp:
        tmpdir = Path(tmp).resolve()

        random_name = uuid.uuid4().hex
        test_file = tmpdir / "test.style.toml"
        with test_file.open("w") as f:
            f.write(f"[['{random_name}.txt'.contains]]\n")
            f.write(f"line = '{random_name}'\n")

        styles = (_ROOT / "styles").glob("**/*.toml")
        presets = (_ROOT / "presets").glob("**/*.toml")
        test = [test_file]
        files = [str(file) for file in itertools.chain(presets, styles, test)]

        config = tmpdir / "pyproject.toml"
        with config.open("w") as c:
            c.write("[tool.nitpick]\n")
            c.write(f"style = {json.dumps(files)}\n")
            c.write("cache = 'never'\n")

        files = run(
            ["nitpick", "ls"], cwd=tmpdir, check=True, stdout=PIPE, text=True
        ).stdout.splitlines()

        if f"{random_name}.txt" not in files:
            raise Exception("Our configuration was not taken into account")
        print("Ok")


def _lint(_: Namespace):
    run(["nitpick", "check"], check=True)


def _format(_: Namespace):
    run(["nitpick", "fix"], check=True)


def _ci(args: Namespace):
    _lint(args)
    _test(args)


def _main():
    parser = ArgumentParser()

    subparsers = parser.add_subparsers(metavar="<action>", required=True)

    subparser = subparsers.add_parser("format", help="Auto format files")
    subparser.set_defaults(action=_format)

    subparser = subparsers.add_parser("lint", help="Lint files")
    subparser.set_defaults(action=_lint)

    subparser = subparsers.add_parser("test", help="Test presets")
    subparser.set_defaults(action=_test)

    subparser = subparsers.add_parser("ci")
    subparser.set_defaults(action=_ci)

    autocomplete(parser)
    args = parser.parse_args()

    args.action(args)


if __name__ == "__main__":
    _main()
